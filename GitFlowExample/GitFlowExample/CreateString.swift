//
//  CreateString.swift
//  GitFlowExample
//
//  Created by 楊信之 on 2016/3/12.
//  Copyright © 2016年 CherriTech. All rights reserved.
//

import UIKit

class CreateString: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
//MARK: - Public Variable
    var str: String!
    
    class func getView(color:UIColor) -> CreateString! {
        
        let view = CreateString(frame: CGRect(x: 50, y: 50, width: 50, height: 50))
        view.backgroundColor = color
        
        return view
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.str = "hahaha string!"
        
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
