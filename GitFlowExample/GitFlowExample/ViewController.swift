//
//  ViewController.swift
//  GitFlowExample
//
//  Created by liaozonglun on 2016/3/2.
//  Copyright © 2016年 CherriTech. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

//MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("阿廖新增Log")
        print("阿廖新增Log")
        print("HAHAXD")
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        view.backgroundColor = UIColor.redColor()
        
        showAlert("HI", message: "Hello")
        
        
        configView()
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        print("View Will Appear")
        
    }
    
//MARK: - @IBAction

    @IBAction func clickAction(sender: AnyObject) {
        
        showAlert("Click", message: "Click")
        
    }
    
//MARK: - View Setting
    func configView() {
        
        let square = CreateString.getView(UIColor.yellowColor())
        square.center = self.view.center
        self.view.addSubview(square)
        
    }
    
    
//MARK: -Function
    func showAlert(title:String, message:String) {
        
    
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        let doneAction      = UIAlertAction(title: "Done", style: UIAlertActionStyle.Default) { (action) -> Void in
            //
            print("Action Done")
        }
        
        alertController.addAction(doneAction)
        
        
        self.presentViewController(alertController, animated: true) { () -> Void in
            //
            print("Alert Shown")
        }
        
        
        
    }
    

}

